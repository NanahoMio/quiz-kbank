package com.example.quizkbank

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class QuizKbankApplication

fun main(args: Array<String>) {
	runApplication<QuizKbankApplication>(*args)
}
